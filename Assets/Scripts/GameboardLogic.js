/*
 *  GameboardLogic
 *  Initializes the gameboard, updates the trays and maintains the state of each
 */

#pragma strict

// public vars
var gameTray : Transform;
var gameCamera : Camera;
var spotLight : Light;


// internal vars
private var boardWidth :int = 8;
private var boardHeight :int = boardWidth;
private var gameBoard : Transform[,] = new Transform[this.boardWidth, this.boardHeight];
private var mainCamera : Camera = null;
private var mainLight : Light = null;
private var cameraHeight : int = 0;
private var gamePieces : List.<Transform> = null;
private var listener : Component = null;
private var numberPlacing : int = 0;
private var placeCallback : String = null;
private var numberClearing : int = 0;
private var clearCallback : String = null;
private var clickCallback : String = null;
private var piecesSwappedCallback : String = null;
private var piecesRaised : int = 0;
private var piecesMoved : int = 0;
private var piecesDropped : int = 0;
private var tray1 : Vector2 = Vector2(0,0);
private var tray2 : Vector2 = Vector2(0,0);
private var trayScript1 : TrayLogic = null;
private var trayScript2 : TrayLogic = null;

function InitializeGameboard(boardWidth : int, boardHeight : int, listener : Component) {
	SetBoardWidth(boardWidth);
	SetBoardHeight(boardHeight);
	SetListener(listener);
}

function DisplayGameboard(callback : String) {
	var trayWidth = (this.gameTray.GetComponent('TrayLogic') as TrayLogic).GetWidth();
	var displayX = null;
	var displayY = null;
	var horizontalOffset = this.boardWidth / 2;
	var verticalOffset = this.boardHeight / 2;
	var horizontalCenterOffset = 1 - this.boardHeight % 2;
	var verticalCenterOffset = 1 - this.boardWidth % 2;
	var trayController : TrayLogic = null;
	
	// instantiate camera
	this.mainCamera = Instantiate(this.gameCamera, Vector3.zero, Quaternion.identity);
	// set cameraHeight based on size of board and fieldOfView angle
	var largestDimension = horizontalOffset > verticalOffset ? horizontalOffset : verticalOffset;
	var halfFieldOfViewRadians = this.mainCamera.fieldOfView / 2 * (Mathf.PI / 180);
	// add a one trayWidth border, and adjust for trayWidth
	var adjacentEdge = (largestDimension + 1) * trayWidth;
	this.cameraHeight = adjacentEdge / Mathf.Tan(halfFieldOfViewRadians);

	this.mainCamera.transform.Translate(0, this.cameraHeight, 0);
	this.mainCamera.transform.Rotate(90, 0, 0);
	// position spotLight just above the camera, pointed down at the gameboard
	this.mainLight = Instantiate(this.spotLight, Vector3.zero, Quaternion.identity);
	this.mainLight.enabled = false;
	this.mainLight.transform.Translate(0, cameraHeight * 1.25, 0);
	this.mainLight.transform.Rotate(90, 0, 0);
	this.mainLight.light.color = Color.white;
	this.mainLight.light.intensity = 4;
	this.mainLight.light.spotAngle = 65;
	this.mainLight.light.range = this.cameraHeight * 2;
	this.mainLight.enabled = true;
	
	// instantiate a gameTray for each space on the gameBoard
	//this.gameBoard = new Array(this.boardWidth);
	for (var x = 0; x < this.boardWidth; x++) {
		//this.gameBoard[x] = new Array(boardHeight);
		for (var y = 0; y < this.boardHeight; y++) {
			// adjust for trayWidth and keep the board centered at 0,0,0
			displayX = (x - horizontalOffset) * trayWidth + horizontalCenterOffset;
			displayY = (y - verticalOffset) * trayWidth + verticalCenterOffset;
			// the camera faces straight down, so Y on the gameboard is Z in global space
			this.gameBoard[x,y] = Instantiate(gameTray, Vector3(displayX, 0, displayY), Quaternion.AngleAxis(90.0, Vector3.right));
			//@TODO: make trayController an array so we don't have to call GetComponent over and over
			trayController = (this.gameBoard[x,y].GetComponent('TrayLogic') as TrayLogic);
			trayController.SetTrayId(Vector2(x,y));
			trayController.SetCameraHeight(this.cameraHeight);
			trayController.SetListener(this);	
			trayController.SetClickCallback('TrayClicked');
			trayController.SetupTray();
		}
	}
	
	NotifyCaller(callback);
}	

function PopulateGameboard(virtualGameboard : int[,], callback : String) {
	this.placeCallback = callback;
	for (var x = 0; x < this.boardWidth; x++) {
		for (var y = 0; y < this.boardHeight; y++) {
			//if (virtualGameboard[x,y] != null) {
				PlaceGamePiece(x, y, this.gamePieces[virtualGameboard[x,y]]);
			//}
		}
	}
}

function PlaceMultipleGamePieces(newPieces : List.<Vector3>, callback : String) {
	this.placeCallback = callback;
	for (var piece : Vector3 in newPieces) {
		PlaceGamePiece(piece.x, piece.y, this.gamePieces[piece.z]);
	}
}

function PlaceGamePiece(x : int, y : int, gamePiece : Transform) {
	this.numberPlacing++;
	(this.gameBoard[x,y].GetComponent('TrayLogic') as TrayLogic).PlaceGamePiece(gamePiece, this.cameraHeight + 1, 'PlaceGamePieceComplete');	
}

function PlaceGamePieceComplete() {
	this.numberPlacing--;
	if (this.numberPlacing == 0) {
		NotifyCaller(this.placeCallback);
	}
}

function ClearAllTrays(callback : String) {
	this.clearCallback = callback;
	for (var x = 0; x < this.boardWidth; x++) {
		for (var y = 0; y < this.boardHeight; y++) {
			ClearTray(Vector2(x, y));
		}
	}
}

function ClearTrays(trays : List.<Vector2>, callback : String) {
	this.clearCallback = callback;
	for (var tray : Vector2 in trays) {
		ClearTray(tray);
	}
}

function ClearTray(trayId : Vector2) {
	numberClearing++;
	(this.gameBoard[trayId.x,trayId.y].GetComponent('TrayLogic') as TrayLogic).ClearTray('ClearTrayComplete');
}

function ClearTrayComplete() {
	numberClearing--;
	if (this.numberClearing == 0) {
		NotifyCaller(this.clearCallback);
	}	
}

function SetBoardWidth(boardWidth : int) {
	if (boardWidth != null) {
		this.boardWidth = boardWidth;
	}
}

function GetBoardWidth() {
	return this.boardWidth;
}

function SetBoardHeight(boardHeight : int) {
	if (boardHeight != null) {
		this.boardHeight = boardHeight;	
	}
}

function GetBoardHeight() {
	return this.boardHeight;
}

function SetGamePieces(gamePieces : List.<Transform>) {
	this.gamePieces = gamePieces;
}

function GetGamePieces() {
	return this.gamePieces;
}

function TrayClicked(trayId : Vector2) {
	if (this.numberPlacing == 0 && this.numberClearing == 0 && this.piecesRaised == 0 && this.piecesMoved == 0 && this.piecesDropped == 0) {
		NotifyCallerWithMessage(this.clickCallback, trayId);
	}
}

function Highlight(trayId : Vector2) {
	(this.gameBoard[trayId.x,trayId.y].GetComponent('TrayLogic') as TrayLogic).Highlight();
}

function RemoveHighlight(trayId : Vector2) {
	(this.gameBoard[trayId.x,trayId.y].GetComponent('TrayLogic') as TrayLogic).RemoveHighlight();
}

function SwapPieces(tray1 : Vector2, tray2 : Vector2, callback : String) {
	this.trayScript1 = this.gameBoard[tray1.x,tray1.y].GetComponent('TrayLogic') as TrayLogic;
	this.trayScript2 = this.gameBoard[tray2.x,tray2.y].GetComponent('TrayLogic') as TrayLogic;
	var tray1Position = this.gameBoard[tray1.x,tray1.y].position;
	var tray2Position = this.gameBoard[tray2.x,tray2.y].position;
	this.tray1 = Vector2(tray1Position.x, tray1Position.z);
	this.tray2 = Vector2(tray2Position.x, tray2Position.z);
	this.piecesSwappedCallback = callback;
	this.piecesRaised = 2;
	// raise one piece high
	(this.gameBoard[tray1.x,tray1.y].GetComponent('TrayLogic') as TrayLogic).RaisePiece(true, 'PieceRaised');
	// raise the other piece low
	(this.gameBoard[tray2.x,tray2.y].GetComponent('TrayLogic') as TrayLogic).RaisePiece(false, 'PieceRaised');
}

function PieceRaised() {
	if (--this.piecesRaised == 0) {
		this.piecesMoved = 2;
		this.trayScript1.MovePiece(this.tray2, 'PieceMoved');
		this.trayScript2.MovePiece(this.tray1, 'PieceMoved');
	}
}

function PieceMoved() {
	if (--this.piecesMoved == 0) {
		this.piecesDropped = 2;
		this.trayScript1.DropPiece('PieceDropped');
		this.trayScript2.DropPiece('PieceDropped');
	}
}

function PieceDropped() {
	if (--this.piecesDropped == 0) {
		var tempGamePiece = this.trayScript1.GetGamePiece();
		this.trayScript1.SetGamePiece(this.trayScript2.GetGamePiece());
		this.trayScript2.SetGamePiece(tempGamePiece);
		this.NotifyCaller(this.piecesSwappedCallback);
	}
}

function SetListener(listener : Component) {
	this.listener = listener;
}

function SetClickCallback(callback : String) {
	this.clickCallback = callback;
}

function NotifyCaller(callback: String) {
	if (this.listener != null && callback != null) {
		this.listener.SendMessage(callback, null);
	}
}

function NotifyCallerWithMessage(callback : String, message : Object) {
	if (this.listener != null && callback != null) {
		this.listener.SendMessage(callback, message);
	}
}