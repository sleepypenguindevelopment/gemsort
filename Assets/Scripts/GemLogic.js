/*
 *  GemLogic
 * 	Attached to any "gem" game pieces to have them clean themselves up
 */

#pragma strict

// public vars
var floor : int = -1;  // The Y value at which the gem will self destruct
var highY : float = 2.0; // The higher Y value to which a piece is raised to be swapped with another
var lowY : float = 0; // The lower Y value to which a piece is raised to be swapped with another
var swapSpeed : int = 10;

// internal vars
private var listener : Component = null;
private var clickCallback : String = null;
private var pieceRaisedCallback : String = null;
private var pieceMovedCallback : String = null;
private var pieceDroppedCallback : String = null;
private var movingDown : boolean = false;
private var movingUp : boolean = false;
private var movingOver : boolean = false;
private var toY : float = 0;
private var destination : Vector2 = Vector2(0,0);

function Start() {
	this.rigidbody.useGravity = false;
}

function FixedUpdate () {
	if (this.transform.position.y < floor) {
		Destroy(this.gameObject);
	}
	
	if (this.movingDown) {
		this.transform.Translate(-1 * Vector3.up * Time.fixedDeltaTime * this.swapSpeed, Space.World);
		if (this.transform.position.y <= 0.5) {
			this.transform.position = new Vector3(transform.position.x, 0.5, transform.position.z);
			this.PieceDown();
		}
	}
	
	if (this.movingUp) {
		this.transform.Translate(Vector3.up * Time.fixedDeltaTime * this.swapSpeed, Space.World);
		if (this.transform.position.y >= toY) {
			movingUp = false;
			this.NotifyCaller(this.pieceRaisedCallback);
		}
	}
		
	if (this.movingOver) {
		var multiplier = Time.fixedDeltaTime * this.swapSpeed;
		var translateX = destination.x - this.transform.position.x;
		var translateY = destination.y - this.transform.position.z;
		this.transform.Translate(translateX * multiplier, 0, translateY * multiplier, Space.World);
		
		translateX = destination.x - this.transform.position.x;
		translateY = destination.y - this.transform.position.z;
		if (Mathf.Abs(translateX) <= 0.1 && Mathf.Abs(translateY) <= 0.1) {
				movingOver = false;
				this.NotifyCaller(this.pieceMovedCallback);
		}
	}	
}

function SetListener(listener : Component) {
	this.listener = listener;
}

function SetClickCallback(callback: String) {
	this.clickCallback = callback;
}

function RaisePiece(high : boolean, callback : String) {
	this.pieceRaisedCallback = callback;
	this.rigidbody.useGravity = false;
	if (high) {
		this.toY = this.highY;
	} else {
		this.toY = this.lowY;
	}
	this.movingUp = true;
}

function MovePiece(destination : Vector2, callback : String) {
	this.pieceMovedCallback = callback;
	this.destination = destination;	
	this.movingOver = true;
}

function PieceMoved() {
	this.NotifyCaller(this.pieceMovedCallback);
}

function DropPiece(callback : String) {
	this.pieceDroppedCallback = callback;
	this.movingDown = true;
}

function OnMouseDown() {
	NotifyCaller(this.clickCallback);
}

function PieceDown() {
	this.movingDown = false;
	this.rigidbody.useGravity = true;
	this.NotifyCaller(this.pieceDroppedCallback);
}

function NotifyCaller(callback: String) {
	if (this.listener != null && callback != null) {
		this.listener.SendMessage(callback, null);
	}
}