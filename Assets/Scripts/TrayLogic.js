/*
 *  TrayLogic
 *  Place and remove gamepieces on this space of the gameboard
 */

#pragma strict

// public vars
public var spotLight : Light;

//internal vars
private var trayId : Vector2;
private var gamePiece : Transform = null;
private var hiLight : Light = null;
private var cameraHeight : int = 0;
private var listener : Component = null;
private var placeCallback : String = null;
private var clearCallback : String = null;
private var clickCallback : String = null;
private var pieceRaisedCallback : String = null;
private var pieceMovedCallback : String = null;
private var pieceDroppedCallback : String = null;
private var placed : boolean = false;
private var dropped : boolean = true;
private var gamePieceController : GemLogic = null;
private var rotating : boolean = false;
private var oldRotation : float = 0f;
private var rotated : float = 0f;

// constants
private var PIECE_TAG : String = "gem";
private var TRAY_WIDTH : int = 2;

function Update() {
	if (rotating) {
		if (rotated < 179) {
			var rotation : float = 180 * Time.deltaTime;
			transform.Rotate(new Vector3(rotation, 0, 0));
			rotated += rotation;
		} else {
			var newRotation : float = oldRotation + 180;
			if (newRotation >= 360) {
				newRotation -= 360;
			}
			transform.eulerAngles.x = newRotation;
			rotated = 0;
			rotating = false;
			ClearComplete();
		}
	}
}

function SetupTray() {
	// position hiLight just above the camera, pointed down at this tray
	this.hiLight = Instantiate(this.spotLight, Vector3.zero, Quaternion.identity);
	this.hiLight.enabled = false;
	this.hiLight.transform.Translate(this.transform.position.x, this.cameraHeight * 1.25, this.transform.position.z);
	this.hiLight.transform.Rotate(90, 0, 0);
	this.hiLight.light.color = Color.yellow;
	this.hiLight.light.intensity = 6;
	this.hiLight.light.spotAngle = 7;
	this.hiLight.light.range = this.cameraHeight * 2;
}

function PlaceGamePiece(gamePiece : Transform, pieceHeight : int, callback : String) {
	var pieceRotation = Quaternion.identity;
	var pieceZ = this.transform.position.z;
	this.placeCallback = callback;
	this.placed = false;
	// adjust for mesh import bug turning meshes 90 degrees
	if (gamePiece.name.IndexOf("diamond") != -1) {
		pieceRotation = Quaternion.AngleAxis(90.0, Vector3.right);
	} else if (gamePiece.name == "emerald" || gamePiece.name == "ruby") {
		pieceRotation = Quaternion.AngleAxis(90.0, Vector3.right);
	}

	this.gamePiece = Instantiate(
		gamePiece, 
		Vector3(this.transform.position.x, pieceHeight, pieceZ), 
		pieceRotation
	);
	InitializeGamePiece();
	this.gamePieceController.DropPiece('PieceDropped');
}

function SetGamePiece(gamePiece : Transform) {
	this.gamePiece = gamePiece;
	InitializeGamePiece();
}

function GetGamePiece() {
	return this.gamePiece;
}

function InitializeGamePiece() {
	this.gamePiece.transform.parent = this.transform;
	this.gamePieceController = (this.gamePiece.GetComponent('GemLogic') as GemLogic);
	this.gamePieceController.SetListener(this);
	this.gamePieceController.SetClickCallback('OnMouseDown');
}

function PieceDropped() {
	if (!this.placed) {
		this.placed = true;
		NotifyCaller(this.placeCallback);
	}
	if (!this.dropped) {
		this.dropped = true;
		NotifyCaller(this.pieceDroppedCallback);
	}
}

function ClearTray(callback: String) {
	this.clearCallback = callback;
	oldRotation = transform.eulerAngles.x;
	rotating = true;
}

function ClearComplete() {
	NotifyCaller(this.clearCallback);
}

function Highlight() {
	this.hiLight.enabled = true;
}

function RemoveHighlight() {
	this.hiLight.enabled = false;
}

function RaisePiece(high : boolean, callback : String) {
	this.pieceRaisedCallback = callback;
	this.gamePieceController.RaisePiece(high, 'PieceRaised');
}

function PieceRaised() {
	this.NotifyCaller(this.pieceRaisedCallback);
}

function MovePiece(destination : Vector2, callback : String) {
	this.pieceMovedCallback = callback;
	this.gamePieceController.MovePiece(destination, 'PieceMoved');
}

function PieceMoved() {
	this.NotifyCaller(this.pieceMovedCallback);
}

function DropPiece(callback : String) {
	this.pieceDroppedCallback = callback;
	this.dropped = false;
	this.gamePieceController.DropPiece('PieceDropped');
}

function SetTrayId(trayId : Vector2) {
	this.trayId = trayId;
}

function SetListener(listener : Component) {
	this.listener = listener;
}

function SetCameraHeight(cameraHeight : int) {
	this.cameraHeight = cameraHeight;
}

function SetClickCallback(callback : String) {
	this.clickCallback = callback;
}

function NotifyCaller(callback : String) {
	if (this.listener != null && callback != null) {
		this.listener.SendMessage(callback, null);
	}
}

function NotifyCallerWithMessage(callback : String, message : Object) {
	if (this.listener != null && callback != null) {
		this.listener.SendMessage(callback, message);
	}
}

function OnMouseDown() {
	if (this.placed && this.dropped && !rotating) {
		NotifyCallerWithMessage(this.clickCallback, this.trayId);
	}
}

function GetWidth() {
	return TRAY_WIDTH;
}
