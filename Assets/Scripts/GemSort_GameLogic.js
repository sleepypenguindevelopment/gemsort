/*
 * GemSort_GameLogic.js
 * 
 * Main logic for the Gem Sort game
 * 
 * Flow:
 * 	Initialize Virtual Gameboard
 * 	Populate Virtual Gameboard
 *	Instantiate actual Gameboard
 * 	Populate actual Gameboard
 *	When user clicks, mark that tray
 * 	When user clicks again, unmark tray or swap gems
 * 	After swapping gems, scan board for matches
 *	If there are matches, flip trays with matching gems
 *	Choose new gems for flipped trays and populate virtual & actual Gameboards 
 *
 */
 
#pragma strict

import System.Collections.Generic;

// public vars
var gameboard : Transform;
var boardWidth :int = 8;
var boardHeight :int = boardWidth;
var gamePieces : List.<Transform> = new List.<Transform>();

// internal vars
private var ready : boolean = false;
private var clearBoard : boolean = false;
private var gameboardInstance : Transform = null;
private var gameboardController : GameboardLogic;
private var gamePieceIndexes = new List.<int>();
private var virtualGameboard : int[,];
private var allMatches : List.<Vector2> = new List.<Vector2>();
private var nullVector : Vector2 = Vector2(this.boardWidth+1, this.boardHeight+1);
private var highlighted : Vector2 = nullVector;
private var secondaryTray : Vector2 = nullVector;
private var clickSwap : boolean = false;
private var score : int = 0;
private var scoreMultiplier : int = 5;

function Start() {
	// Instantiate Actual Gameboard
	this.gameboardInstance = Instantiate(this.gameboard, Vector3.zero, Quaternion.identity);
	this.gameboardController = (this.gameboardInstance.GetComponent('GameboardLogic') as GameboardLogic);
	this.gameboardController.InitializeGameboard(this.boardWidth, this.boardHeight, this);
	this.gameboardController.SetGamePieces(this.gamePieces);
	this.gameboardController.SetClickCallback('TrayClicked');
	
	// Initialize Virtual Gameboard 
	InitializeVirtualGameboard();
	
	// Display Actual Gameboard
	this.gameboardController.DisplayGameboard('GameboardDisplayed');
}

function Update () {

	if (ready) {
 		// After swapping gems, scan board for matches
 		// If there are matches, flip trays with matching gems
 		// Choose new gems for flipped trays and populate virtual & actual Gameboards 
 		
		if (clearBoard) {
			this.ready = false;
			this.gameboardController.ClearAllTrays('ClearingAllComplete');
		}
	}
}

function OnGUI () {
	GUI.Label(Rect (Screen.width - 110,40,100,20), this.score.ToString());
}

function TrayClicked(trayId : Vector2) {
	// When user clicks, mark that tray
	if (trayId == this.highlighted) {
		// When user clicks again, unmark tray
		this.gameboardController.RemoveHighlight(trayId);
		this.highlighted = this.nullVector;
	} else {	 	
		if (this.highlighted != this.nullVector) {
			// User has clicked a second tray, remove highlight
			this.gameboardController.RemoveHighlight(highlighted);
			// if second tray is adjacent to first, swap game pieces
			if(Vector2.Distance(trayId, this.highlighted) < 2) {
				this.clickSwap = true;
				this.secondaryTray = this.highlighted;
				this.SwapPieces(this.highlighted, trayId);
			}
		}
		this.gameboardController.Highlight(trayId);
		this.highlighted = trayId;
	}
}

function SwapPieces(tray1 : Vector2, tray2 : Vector2) {
	this.gameboardController.SwapPieces(tray1, tray2, 'PiecesSwapped');
}

function PiecesSwapped() {
	// Pieces have been swapped, now swap virtual pieces
	var tempTray : int = this.virtualGameboard[this.highlighted.x, this.highlighted.y];
	this.virtualGameboard[this.highlighted.x, this.highlighted.y] = this.virtualGameboard[this.secondaryTray.x, this.secondaryTray.y];
	this.virtualGameboard[this.secondaryTray.x, this.secondaryTray.y] = tempTray;
	
	// scan for matches
	ScanBoard();
	if (this.allMatches.Count > 0) {
		// remove highlight
		this.gameboardController.RemoveHighlight(this.highlighted);
		this.highlighted = this.nullVector;
		// replace matches in virtualGameboard with new random gems
		this.gameboardController.ClearTrays(this.allMatches, 'ClearingComplete');
	} else {
		if (this.clickSwap) {
			this.clickSwap = false;
			this.SwapPieces(this.highlighted, this.secondaryTray);
		}
	}
	ready = true;
}

function InitializeVirtualGameboard() {	
	if (this.boardWidth > 0 && this.boardHeight > 0) {
		this.virtualGameboard = new int[this.boardWidth, this.boardHeight];
	} else {
		throw("Game board must not have dimension of zero: width = " + boardWidth + " height = " + boardHeight);
	}
	this.gamePieceIndexes = this.GetGamePieceIndexes();
	var gamePieceCount = this.gamePieceIndexes.Count;
	if (gamePieceCount > 0) {
		// randomize pieces and populate board
		for (var x = 0; x < this.boardWidth; x++) {
			for (var y = 0; y < this.boardHeight; y++) {
				this.virtualGameboard[x,y] = this.gamePieceIndexes[Random.Range(0, gamePieceCount)];
			}
		}
	} else {
		throw("No Game Pieces");
	}

	// scan board until there are no matches
	ScanBoard();
	var loops : int = 0;
	var maxLoops : int = 1000;
	while (this.allMatches.Count > 0 && loops < maxLoops) {
		// replace matches in virtualGameboard with new random gems
		for (var match : Vector2 in this.allMatches) {
			this.virtualGameboard[match.x, match.y] = this.gamePieceIndexes[Random.Range(0,gamePieceCount)];
		}	
		// re-scan
		ScanBoard();
		// make sure while loop doesn't run forever
		loops++;
	}
}

function ScanBoard() {
	var matchList : List.<Vector2> = new List.<Vector2>();
	this.allMatches = new List.<Vector2>();
	
	for (var x = 0; x < this.boardWidth; x++) {
		for (var y = 0; y < this.boardHeight; y++) {
			// x-1 & y-1 have already been checked
			
			// check x+1
			ScanVector(x, y, Vector2(1,0));

			// check y+1
			ScanVector(x, y, Vector2(0,1));		
			
			// check x+1, y+1
			//ScanVector(x, y, Vector2(1,1));		
			
			// check x+1, y-1
			//ScanVector(x, y, Vector2(1,-1));	
		}
	}
}

// If 3 or more matched on this vector,
// add them to collection of allMatches
function ScanVector(x : int, y : int, v : Vector2) {
	var matchList : List.<Vector2> = new List.<Vector2>();
	matchList = Check(x, y, v);
	// are there at least 3 in a row?
	if (matchList.Count > 2) {
		// if not already added, add them to allMatches
		for (var match : Vector2 in matchList) {
			if (!this.allMatches.Contains(match)) {
				this.allMatches.Add(match);
			}
		}
	}
}

// Recursively check for matches along a particular vector
function Check(m : int, n : int, v : Vector2) : List.<Vector2> {
	var matches : List.<Vector2> = new List.<Vector2>();
	var matchList : List.<Vector2> = new List.<Vector2>();
	var newPosition : Vector2 = Vector2(m, n) + v;
	
	matches.Add(Vector2(m,n));
	if (newPosition.x < this.boardWidth && newPosition.y < this.boardHeight 
		&& this.virtualGameboard[m,n] == this.virtualGameboard[newPosition.x, newPosition.y]) {
		matchList = Check(newPosition.x, newPosition.y, v);
		if (matchList.Count > 0) {
			matches.AddRange(matchList);
		}
	}
	return matches;
}

function GameboardDisplayed() {
	// Populate Actual Gameboard
	this.ClearingAllComplete();
}

function ClearingComplete() {
	var matchUpdates : List.<Vector3> = new List.<Vector3>();
	var gamePieceCount : int = this.gamePieceIndexes.Count;
	
	UpdateScore(allMatches.Count);
	
	// update virtualGameboard
	for (var match : Vector2 in this.allMatches) {
		this.virtualGameboard[match.x, match.y] = this.gamePieceIndexes[Random.Range(0,gamePieceCount)];
		matchUpdates.Add(Vector3(match.x, match.y, this.virtualGameboard[match.x, match.y]));
	}
	// update actual gameBoard	
	this.gameboardController.PlaceMultipleGamePieces(matchUpdates, 'PopulateGameboardComplete');
}

function ClearingAllComplete() {
	this.gameboardController.PopulateGameboard(virtualGameboard, 'PopulateGameboardComplete');
}

function UpdateScore(matchCount : int) {

Debug.Log(matchCount);
	for (var i = 1; i <= matchCount; i++) {
		// pause before updating score so player can see it counting up
		yield WaitForSeconds(0.2);
		if (i < 4) {	
			this.score += this.scoreMultiplier; 
		} else {		
			this.score += i * this.scoreMultiplier;
		}
	}
}

function PopulateGameboardComplete() {
	// re-scan
	ScanBoard();
	if (this.allMatches.Count > 0) {
		// replace matches in virtualGameboard with new random gems
		this.gameboardController.ClearTrays(this.allMatches, 'ClearingComplete');
	} else {
		this.ready = true;
	}
}

function GetGamePieceIndexes() {
	var gamePieceIndexes = new System.Collections.Generic.List.<int>();
	for (var i = 0; i < this.gamePieces.Count; i++) {
		//gamePieceIndexes[i] = i;
		gamePieceIndexes.Add(i);
	}
	return gamePieceIndexes;
}