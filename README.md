# GemSort #

*GemSort* (aka *Gypsy's Gems*) was my first Unity project.  It's a match 3 game using 3D models.  Future plans include using the accelerometer on a mobile platform to control which direction gems slide after matched gems are removed.

It also includes my first Blender 3D model (the "trays" that make up the gameboard).

Since I had not yet learned about coroutines (or perhaps they hadn't been added to Unity when I started), it makes extensive use of callbacks. 

### This is a work in progress. ###
